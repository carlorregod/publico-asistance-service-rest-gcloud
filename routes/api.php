<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Users Section */
//Todo testeado y funcionando en el ambiente del proyecto
Route::group(['prefix' => 'users'], function () {
    Route::get('/{rut}', 'UsuarioController@show')
        ->where(['rut' => '[0-9kK]+']); //OK!
    Route::post('/login', 'UsuarioController@login');//OK!
    Route::put('/newpassword', 'UsuarioController@newPassword'); //OK!!
    Route::post('/registry', 'RegistroAsistenciaController@registry'); //OK!
    Route::get('/lastRegistry/{rut}', 'UsuarioController@lastRegister')
        ->where(['rut' => '[0-9kK]+']);  //OK!
    Route::get('/firstRegistry/{rut}', 'RegistroAsistenciaController@firstRegistry')
        ->where(['rut' => '[0-9kK]+']); //OK!
    Route::post('/serchRegistry', 'RegistroAsistenciaController@searchRegistry');//OK!
});
