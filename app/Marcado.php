<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marcado extends Model
{
    //
    protected $table = 'Marcado';

    public $timestamps = false;

    public $primaryKey = 'id_marcado';
}
