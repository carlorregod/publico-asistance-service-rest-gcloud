<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registro_Asistencia extends Model
{
    protected $table = 'Registro_Asistencia';

    public $timestamps = false;

    protected $primaryKey ='contador';

    protected $fillable = ['RUT', 'id_marcado', 'id_celular', 'estado_ausentismo', 'Fecha_marcado', 'Hora_marca', 'coordX_GPS', 'coordY_GPS'];

    protected $foreignKey = ['RUT', 'id_marcado', 'id_celular'];
}
