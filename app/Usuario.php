<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    //
    protected $table = 'Usuario';

    public $timestamps = false;

    public $primaryKey = 'RUT';

    public $incrementing = false;
}
