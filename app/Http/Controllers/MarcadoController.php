<?php

namespace App\Http\Controllers;

use App\Marcado;
use Illuminate\Http\Request;

class MarcadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Marcado  $marcado
     * @return \Illuminate\Http\Response
     */
    public function show(Marcado $marcado)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Marcado  $marcado
     * @return \Illuminate\Http\Response
     */
    public function edit(Marcado $marcado)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Marcado  $marcado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marcado $marcado)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Marcado  $marcado
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marcado $marcado)
    {
        //
    }
}
