<?php

namespace App\Http\Controllers;

use App\Celular;
use App\Usuario;
use App\Registro_Asistencia;
use App\Marcado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function show($rut)
    {
        $rut = preg_replace('/[\.\-]/i', '', $rut);
        $user = Usuario::where('RUT', $rut)
            ->join('Celular','Telefono','=','Celular.id_celular')
            ->first();         
        //$rut = substr($rut, 0, strlen($rut) - 1);
        $result = ($user === null) ? false : true ;
        return response()->json([
            'status' => 200,
            'result' => $result,
            'user' => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function edit(Usuario $usuario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Usuario $usuario)
    {
        //
    }

    public function login(Request $request)
    {
        $response = json_encode($this->validarRut($request->rut)->original);
        $response = json_decode($response);
        if($response->result) {
            $rut = intval($response->rut);
            $user = Usuario::where('RUT', $rut)->first();
            if ($user != null) {
                $response = (Hash::check($request->password, $user['password'])) ? true : false ;

                return response()->json([
                    'status' => 200,
                    'result' => $response,
                    'rut' => $request->rut
                ]);
            } else {
                $response->result = false;
            }
        }

        return response()->json(
            ['status' => 200,
            'result' => $response->result]);
    }

    public function newPassword(Request $request)
    {
        $message = '';
        $response = json_encode($this->validarRut($request->rut)->original);
        $response = json_decode($response);
        $rut = $response->rut;

        $user = Usuario::where('RUT', $rut)->first();

        $correctPassword = (Hash::check($request->password, $user['password'])) ? true : false ;

        if ($correctPassword) {
            $user->password = bcrypt($request->newPassword);
            $user->save();
        }else {
            $message = 'La contraseña ingresada no es válida.';
        }
        return response()->json([
            'status' => 200,
            'result' => $correctPassword,
            'message' => $message
            ]);
    }

    public function validarRut($rut) {
        $response = false;
        if (!preg_match("/^[0-9.]+[-]?+[0-9kK]{1}/", $rut)) {
        }else {
            $rut = preg_replace('/[\.\-]/i', '', $rut);
            $dv = substr($rut, -1);
            $numero = substr($rut, 0, strlen($rut) - 1);
            $i = 2;
            $suma = 0;
            foreach (array_reverse(str_split($numero)) as $v) {
                if ($i == 8) {
                    $i = 2;
                }
                $suma += $v * $i;
                ++$i;
            }
            $dvr = 11 - ($suma % 11);
            if ($dvr == 11) {
                $dvr = 0;
            }
            if ($dvr == 10) {
                $dvr = 'K';
            }
            //En el siguiente Json, se debe estar consciente que se buscará por $rut no $numero.
            if ($dvr == strtoupper($dv)) {
                return response()->json([
                    'result' => true,
                    'rut' => $rut
                ]);
            }
        }

        return response()->json([
            'result' => false,
            'rut' => ''
        ]);
    }

    public function lastRegister($rut) {
        //$response = json_encode($this->validarRut($rut)->original);
        
        //$response = json_decode($response);

        $last_register = Registro_Asistencia::where('RUT', $rut)
                            ->orderBy('Fecha_marcado', 'desc')
                            ->orderBy('Hora_marca', 'desc')
                            ->first();
        if ($last_register !== null) {
            $status = 0;
            switch ($last_register->id_marcado) {
                case 1:
                    $status = 1;
                    break;

                case 2:
                    $status = 0;
                    break;

                case 3:
                    $status = 2;
                    break;

                case 4:
                    $status = 3;
                    break;

                default:
                    $status = 0;
                    break;
            }

            return $status;
        } else {
            return 0;
        }

    }
}
