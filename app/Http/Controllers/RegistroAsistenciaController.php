<?php

namespace App\Http\Controllers;

use App\Registro_Asistencia;
use Illuminate\Http\Request;
use App\Usuario;
use App\Celular;
use App\Marcado;
use Illuminate\Support\Facades\DB;

class RegistroAsistenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Registro_Asistencia  $registro_Asistencia
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Registro_Asistencia  $registro_Asistencia
     * @return \Illuminate\Http\Response
     */
    public function edit(Registro_Asistencia $registro_Asistencia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Registro_Asistencia  $registro_Asistencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Registro_Asistencia $registro_Asistencia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Registro_Asistencia  $registro_Asistencia
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function registry(Request $request) {
        
        $rut = preg_replace('/[\.\-]/i', '', $request->rut);
        //$rut = substr($rut, 0, strlen($rut) - 1);
        
        if (Usuario::where('RUT', $rut) !== null) {
            if (Marcado::where('id_marcado', $request->id_marcado)) {
                if ($request->coordX !== null || $request->coordX !== '' || $request->coordY !== null || $request->coordY !== '') {
                    if ($request->imei !== null) {
                        $celular = Celular::where('IMEI', $request->imei)->first();
                        if ($celular === null) {
                            $celular = Celular::create([
                                'Numero_celular' => $num = (empty($request->num_celular)) ? 0 : $request->num_celular,
                                'Compania' => $compania = (empty($request->compania)) ? " " : $request->compania,
                                'IMEI' => $request->imei
                            ]);
                            $celular = Celular::where('IMEI', $request->imei)->first();
                        }
                        $marcado = 0;

                        switch ($request->id_marcado) {
                            case 0:
                                $marcado = 2;
                                break;

                            case 1:
                                $marcado = 1;
                                break;

                            case 2:
                                $marcado = 3;
                                break;

                            case 3:
                                $marcado = 4;
                                break;

                            default:
                                $marcado = 2;
                                break;
                        }

                        date_default_timezone_set('America/Santiago');
                        $fecha = date('Y-m-d');
                        $hora = date('G:i:s');

                        $registro = Registro_Asistencia::create([
                            'RUT' => $rut,
                            'id_marcado' => $marcado,
                            'id_celular' => $celular->id_celular,
                            'estado_ausentismo' => 'Presente',
                            'Fecha_marcado' => $fecha,
                            'Hora_marca' => $hora,
                            'coordX_GPS' => $request->coordX,
                            'coordY_GPS' => $request->coordY
                        ]);

                        $result = ($registro !== null) ? true : false ;

                        return response()->json([
                            'result' => $result,
                            'marcado' => $request->id_marcado
                        ]);
                    }
                }
            }
        }

        return response()->json([
            'result' => false
        ]);
    }

    public function firstRegistry($rut) {
        
        $rut = preg_replace('/[\.\-]/i', '', $rut);
        //$rut = substr($rut, 0, strlen($rut) - 1);
        
        $registro = DB::table('Registro_Asistencia')
                        //->select(DB::raw('cast(date_part(\'year\', "Fecha_marcado") as INT) as year, cast(date_part(\'month\', "Fecha_marcado") as INT) as month'))
                        ->select(
                            DB::RAW('YEAR(Fecha_marcado) AS year, MONTH(Fecha_marcado) AS month')
                            )
                        ->where('RUT', $rut)
                        ->orderBy('Fecha_marcado', 'asc')
                        ->first();
        if ($registro === null) {
            return response()->json([
                'result' => false
            ]);
        } else {
            return response()->json([
                'result' => true,
                'year' => $registro->year,
                'month' => $registro->month,
            ]);
        }
    }

    public function searchRegistry(Request $request) {
        $rut = $request->rut;
        $rut = preg_replace('/[\.\-]/i', '', $request->rut);
        //Descomentar para bśuquedas sin dv.
        //$rut = substr($rut, 0, strlen($rut) - 1);
        $dia = [];
        $ciclo = [];
        $fecha = "";
        $entrada = "";
        $entradaX = "";
        $entradaY = "";
        $entradaColacion = "";
        $entradaColacionX = "";
        $entradaColacionY = "";
        $salida = "";
        $salidaX = "";
        $salidaY = "";
        $salidaColacion = "";
        $salidaColacionX = "";
        $salidaColacionY = "";
        $numberRegister = 1;
        $registro = Registro_Asistencia::where('RUT', $rut)
                        //->whereRaw('date_part(\'year\', "Fecha_marcado") = ?', [$request->year])
                        //->whereRaw('date_part(\'month\', "Fecha_marcado") = ?', [$request->month])
                        ->whereRaw('YEAR(Fecha_marcado) = ?',[$request->year])
                        ->whereRaw('MONTH(Fecha_marcado) = ?',[$request->month])
                        ->orderBy('Fecha_marcado', 'desc')
                        ->orderBy('Hora_marca', 'asc')
                        ->get();
        if (strlen($registro) !== 2) {
            foreach ($registro as $r) {
                if ($fecha !== $r->Fecha_marcado) {
                    if (($entrada !== "" || $entradaColacion !== "" || $salidaColacion !== "" || $salida !== "") && $fecha !== "") {
                        $reg = array("entrada" => array("hora" => $entrada, "coordX" => $entradaX, "coordY" => $entradaY),
                                    "salida" => array("hora" => $salida, "coordX" => $salidaX, "coordY" => $salidaY),
                                    "entradaColacion" => array("hora" => $entradaColacion, "coordX" => $entradaColacionX, "coordY" => $entradaColacionY),
                                    "salidaColacion" => array("hora" => $salidaColacion, "coordX" => $salidaColacionX, "coordY" => $salidaColacionY));
                        array_push($ciclo, $reg);
                        $regF = array("fecha" => $fecha, "ciclos" => $ciclo);
                        array_push($dia, $regF);
                        $entrada = "";
                        $entradaX = "";
                        $entradaY = "";
                        $entradaColacion = "";
                        $entradaColacionX = "";
                        $entradaColacionY = "";
                        $salida = "";
                        $salidaX = "";
                        $salidaY = "";
                        $salidaColacion = "";
                        $salidaColacionX = "";
                        $salidaColacionY = "";
                        $ciclo = [];
                        $numberRegister = 1;
                    }
                }

                $fecha = $r->Fecha_marcado;
                switch ($r->id_marcado) {
                    case 1:
                        if (($entrada !== "" || $entradaColacion !== "" || $salidaColacion !== "" || $salida !== "") && $fecha !== "") {
                            $reg = array("entrada" => array("hora" => $entrada, "coordX" => $entradaX, "coordY" => $entradaY),
                                        "salida" => array("hora" => $salida, "coordX" => $salidaX, "coordY" => $salidaY),
                                        "entradaColacion" => array("hora" => $entradaColacion, "coordX" => $entradaColacionX, "coordY" => $entradaColacionY),
                                        "salidaColacion" => array("hora" => $salidaColacion, "coordX" => $salidaColacionX, "coordY" => $salidaColacionY));
                            array_push($ciclo, $reg);
                            $entrada = "";
                            $entradaX = "";
                            $entradaY = "";
                            $entradaColacion = "";
                            $entradaColacionX = "";
                            $entradaColacionY = "";
                            $salida = "";
                            $salidaX = "";
                            $salidaY = "";
                            $salidaColacion = "";
                            $salidaColacionX = "";
                            $salidaColacionY = "";
                            $numberRegister++;
                        }
                        $entrada = $r->Hora_marca;
                        $entradaX = $r->coordX_GPS;
                        $entradaY = $r->coordY_GPS;
                        break;

                    case 2:
                        $salida = $r->Hora_marca;
                        $salidaX = $r->coordX_GPS;
                        $salidaY = $r->coordY_GPS;
                        break;

                    case 3:
                        $entradaColacion = $r->Hora_marca;
                        $entradaColacionX = $r->coordX_GPS;
                        $entradaColacionY = $r->coordY_GPS;
                        break;

                    case 4:
                        $salidaColacion = $r->Hora_marca;
                        $salidaColacionX = $r->coordX_GPS;
                        $salidaColacionY = $r->coordY_GPS;
                        break;

                    default:
                        break;
                }
            }

            if (($entrada !== "" || $entradaColacion !== "" || $salidaColacion !== "" || $salida !== "") && $fecha !== "") {
                $reg = array("entrada" => array("hora" => $entrada, "coordX" => $entradaX, "coordY" => $entradaY),
                            "salida" => array("hora" => $salida, "coordX" => $salidaX, "coordY" => $salidaY),
                            "entradaColacion" => array("hora" => $entradaColacion, "coordX" => $entradaColacionX, "coordY" => $entradaColacionY),
                            "salidaColacion" => array("hora" => $salidaColacion, "coordX" => $salidaColacionX, "coordY" => $salidaColacionY));
                array_push($ciclo, $reg);
                $regF = array("fecha" => $fecha, "ciclos" => $ciclo);
                array_push($dia, $regF);
            }

            return response()->json([
                'result' => true,
                'registro' => $dia
            ]);
        }else {
            return response()->json([
                'result' => false,
                'registro' => $dia
            ]);
        }
    }
}
